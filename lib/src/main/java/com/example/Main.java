package com.example;

import java.lang.reflect.Proxy;

/**
 * Created by robert on 16/6/8.
 */
public class Main {

	public static void main (String[] args) {
		/*Laptop 延迟到客户去实现 */
		Laptop laptop = new Laptop ();
		Handler handler = new Handler (laptop);
		IComputer computer = (IComputer) Proxy.newProxyInstance (laptop.getClass ().getClassLoader (), laptop.getClass ().getInterfaces (), handler);
		computer.execute ();
	}
}
