# 我的其他仓库推荐

|分类（lib-android）|-描述-|-仓库链接-|
|-----|:-----:|:------|
|LoadingLayout|Android 业务开发常用的 loadinglayout 用于封装加载过程中,加载完成&有数据,加载完成&无数据,加载错误的情况。|https://git.oschina.net/alpha4/lib-android-LoadingLayout|
|VerifyCollect|Android 业务开发常用的 verify-collect 用于封装 验证 过程。|https://git.oschina.net/alpha4/lib-android-VerifyCollect|
|PermissionHelper|Android M 权限申请|https://git.oschina.net/alpha4/lib-android-PermissionHelper|
|Social|Android 第三方社会化分享、登录|https://git.oschina.net/alpha4/lib-android-Social|

|分类（完整开源项目）|-描述-|-仓库链接-|
|-----|:-----:|:------|
|Tutur（音乐家教）|音乐家教B&C端业务实现|https://git.oschina.net/alpha4/Tutor|


#lib-android-VerifyCollect

Android 业务开发常用的 verify-collect 用于封装 `验证` 过程。

有任何建议或反馈 [请联系: chenjunqi.china@gmail.com](mailto:chenjunqi.china@gmail.com) 

欢迎大家加入`android 开源项目群(369194705)`, 有合适的项目大家一起 `fork`;

# 使用场景
1. 单个职责验证
2. 串行职责验证(验证顺序为：构造链的顺序)

# 代码

```

		void verifyRegister () {
        
        		String username = editText_username.getText ().toString ();
        		String idCard = editText_idcard.getText ().toString ();
        		String passwd = editText_passwd.getText ().toString ();
        
        
        		new VerifyBuilder ()
        				.append (
        						new VerifyCn (
        								new VerifyCn.Callback () {
        									@Override
        									public void onIllegal (String value) {
        										toast (value + "--> 非中文字符");
        									}
        								}, username))
        				.append (
        						new VerifyRealname (
        								new VerifyRealname.Callback () {
        									@Override
        									public void onTooLong (String value) {
        										toast (value + "--> 姓名过长");
        									}
        								}, username))
        				.append (
        						new VerifyIDCard (
        								new VerifyIDCard.Callback () {
        									@Override
        									public void onEmpty (String value) {
        										toast (value + "--> 为空");
        									}
        
        									@Override
        									public void onIllegal (String value) {
        										toast (value + "--> 身份证格式非法");
        									}
        								}, idCard))
        				.append (
        						new VerifyPassword (
        								new VerifyPassword.Callback () {
        									@Override
        									public void onIllegal (String value) {
        										toast (value + "--> 密码格式非法");
        									}
        								}, passwd))
        				.execute (
        						new ISuccessCallback () {
        							@Override
        							public void onSuccess () {
        								toast ("验证通过");
        							}
        						});
        
        	}

```
# 截图
![screenshot.png](screenshot.png)