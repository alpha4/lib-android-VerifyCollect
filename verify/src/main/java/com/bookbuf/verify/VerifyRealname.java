package com.bookbuf.verify;

import com.bookbuf.verify.base.IVerifyCallback;
import com.bookbuf.verify.base.Verify;

/**
 * Created by robert on 15/11/6.
 */
public class VerifyRealname extends Verify<VerifyRealname.Callback> {

	private String value;

	public VerifyRealname (Callback callback, String value) {
		super (callback);
		this.value = value;
	}

	@Override
	protected int verify () {
		return VerifyImpl.verifyRealName (value);
	}

	@Override
	protected void onFailedCallback (int code) {
		switch (code) {
			case VerifyCode.BS_NAME_TOO_LONG:
				callback.onTooLong (value);
				break;
			default:
				break;
		}
	}


	public interface Callback extends IVerifyCallback {
		void onTooLong (String value);
	}
}
