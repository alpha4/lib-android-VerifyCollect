package com.bookbuf.verify;

import com.bookbuf.verify.base.IVerifyCallback;
import com.bookbuf.verify.base.Verify;

/**
 * Created by robert on 15/11/7.
 */
public class VerifyNotEmpty extends Verify<VerifyNotEmpty.Callback> {
	private String value;

	public VerifyNotEmpty (Callback callback, String value) {
		super (callback);
		this.value = value;
	}

	@Override
	protected int verify () {
		return VerifyImpl.verifyNotEmpty (value);
	}

	@Override
	protected void onFailedCallback (int code) {
		if (code == VerifyCode.BS_VERIFY_EMPTY) {
			callback.onEmpty (value);
		}
	}

	public interface Callback extends IVerifyCallback {
		void onEmpty (String value);
	}
}
