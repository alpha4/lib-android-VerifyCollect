package com.bookbuf.verify.idcard;

import java.util.Calendar;
import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by robert on 15/10/28.
 */
@Deprecated
public class IDCardUtil {
	/**
	 * 功能：设置地区编码
	 *
	 * @return Hashtable 对象
	 */
	public static Hashtable<String, String> GetAreaCode () {
		Hashtable<String, String> hashtable = new Hashtable<> ();
		hashtable.put ("11", "北京");
		hashtable.put ("12", "天津");
		hashtable.put ("13", "河北");
		hashtable.put ("14", "山西");
		hashtable.put ("15", "内蒙古");
		hashtable.put ("21", "辽宁");
		hashtable.put ("22", "吉林");
		hashtable.put ("23", "黑龙江");
		hashtable.put ("31", "上海");
		hashtable.put ("32", "江苏");
		hashtable.put ("33", "浙江");
		hashtable.put ("34", "安徽");
		hashtable.put ("35", "福建");
		hashtable.put ("36", "江西");
		hashtable.put ("37", "山东");
		hashtable.put ("41", "河南");
		hashtable.put ("42", "湖北");
		hashtable.put ("43", "湖南");
		hashtable.put ("44", "广东");
		hashtable.put ("45", "广西");
		hashtable.put ("46", "海南");
		hashtable.put ("50", "重庆");
		hashtable.put ("51", "四川");
		hashtable.put ("52", "贵州");
		hashtable.put ("53", "云南");
		hashtable.put ("54", "西藏");
		hashtable.put ("61", "陕西");
		hashtable.put ("62", "甘肃");
		hashtable.put ("63", "青海");
		hashtable.put ("64", "宁夏");
		hashtable.put ("65", "新疆");
		hashtable.put ("71", "台湾");
		hashtable.put ("81", "香港");
		hashtable.put ("82", "澳门");
		hashtable.put ("91", "国外");
		return hashtable;
	}

	/**
	 * 功能：判断字符串是否为日期格式
	 *
	 * @param value 日期格式为: yyyy-MM-dd
	 * @return
	 */
	public static boolean isDate (String value) {
		Pattern pattern = Pattern
				.compile ("^((\\d{2}(([02468][048])|([13579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\\s(((0?[0-9])|([1-2][0-3]))\\:([0-5]?[0-9])((\\s)|(\\:([0-5]?[0-9])))))?$");
		Matcher m = pattern.matcher (value);
		return m.matches ();
	}

	/**
	 * 功能:在判定已经是正确的身份证号码之后,查找出身份证所在地区
	 * idCard 身份证号码
	 * 所在地区
	 */
	public static String GetArea (String idCard) {
		Hashtable<String, String> ht = GetAreaCode ();
		return ht.get (idCard.substring (0, 2));
	}

	public static boolean isMale (String idCard) {

		String sex = "";
		if (idCard.length () == 18)
			sex = idCard.substring (idCard.length () - 4, idCard.length () - 1);

		int sexNum = Integer.parseInt (sex) % 2;
		return sexNum != 0;
	}

	/**
	 * 功能:在判定已经是正确的身份证号码之后,查找出此人出生日期
	 * idCard 身份证号码
	 * 出生日期 yyyy-MM-DD
	 */
	public static String GetBirthday (String idCard) {
		String Ain = "";
		if (idCard.length () == 18) {
			Ain = idCard.substring (0, 17);
		}
		// ================ 出生年月是否有效 ================
		String strYear = Ain.substring (6, 10);// 年份
		String strMonth = Ain.substring (10, 12);// 月份
		String strDay = Ain.substring (12, 14);// 日期
		return strYear + "-" + strMonth + "-" + strDay;
	}

	public static String getAge (String idCard) {
		String strYear = idCard.substring (6, 10);
		String age = "0";
		Calendar nowCalendar = Calendar.getInstance ();
		nowCalendar.setTimeInMillis (System.currentTimeMillis ());
		// 防止转换出错
		try {
			int tmp = nowCalendar.get (Calendar.YEAR) - Integer.parseInt (strYear);
			if (tmp < 0) {
				return age;
			}
			return tmp + "";
		} catch (Exception e) {
			return age;
		}
	}
}
