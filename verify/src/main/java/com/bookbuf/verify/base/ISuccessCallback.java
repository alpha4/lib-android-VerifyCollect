package com.bookbuf.verify.base;

/**
 * Created by robert on 16/6/8.
 */
public interface ISuccessCallback extends IVerifyCallback {

	void onSuccess ();
}
