package com.bookbuf.verify.base;

/**
 * 接口：职责链
 */
interface IChain<Handler> {
	void setNextVerify (Handler handler);
}
