package com.bookbuf.verify;

import com.bookbuf.verify.base.IVerifyCallback;
import com.bookbuf.verify.base.Verify;

/**
 * 验证是否为中文
 */
public class VerifyCn extends Verify<VerifyCn.Callback> {

	private final String value;


	public VerifyCn (Callback callback, String value) {
		super (callback);
		this.value = value;
	}

	@Override
	protected int verify () {
		return VerifyImpl.verifyCn (value);
	}

	@Override
	protected void onFailedCallback (int code) {
		switch (code) {
			case VerifyCode.BS_CN_ILLAGE:
				callback.onIllegal (value);
				break;
			default:
				break;
		}
	}

	public interface Callback extends IVerifyCallback {

		/*名字非法*/
		void onIllegal (String value);
	}
}
