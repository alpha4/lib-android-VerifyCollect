package com.bookbuf.verify;

import com.bookbuf.verify.base.IVerifyCallback;
import com.bookbuf.verify.base.Verify;

public class VerifyTelephone extends Verify<VerifyTelephone.Callback> {

	public VerifyTelephone (Callback callback, String mobile) {
		super (callback);
		this.mobile = mobile;
	}

	private String mobile;

	@Override
	protected int verify () {
		return VerifyImpl.verifyTelephone (mobile);
	}

	@Override
	protected void onFailedCallback (int code) {
		if (code == VerifyCode.BS_VERIFY_TELEPHONE_ILLEGAL) {
			callback.onIllegal (mobile);
		}
	}


	public interface Callback extends IVerifyCallback {
		void onIllegal (String value);
	}
}
