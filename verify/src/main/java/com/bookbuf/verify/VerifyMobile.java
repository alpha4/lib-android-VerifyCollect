package com.bookbuf.verify;

import com.bookbuf.verify.base.IVerifyCallback;
import com.bookbuf.verify.base.Verify;

/**
 * Created by robert on 15/11/6.
 */
public class VerifyMobile extends Verify<VerifyMobile.Callback> {

	private String mobile;

	public VerifyMobile (Callback callback, String mobile) {
		super (callback);
		this.mobile = mobile;
	}

	@Override
	protected int verify () {
		return VerifyImpl.verifyPhone (mobile);
	}

	@Override
	protected void onFailedCallback (int code) {
		if (code == VerifyCode.BS_VERIFY_PHONE_ILLEGAL) {
			callback.onIllegal (mobile);
		}
	}

	public interface Callback extends IVerifyCallback {
		void onIllegal (String value);
	}
}
