package com.bookbuf.lib_android_verifycollect;

import com.bookbuf.verify.VerifyCn;
import com.bookbuf.verify.VerifyIDCard;
import com.bookbuf.verify.VerifyPassword;
import com.bookbuf.verify.VerifyRealname;
import com.bookbuf.verify.base.ISuccessCallback;
import com.bookbuf.verify.base.VerifyBuilder;

import org.junit.Test;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
	@Test
	public void verifyRegister () throws Exception {

		String username = "陈4俊xx12";
		String idCard = "321282199207123234";
		String passwd = "123&&&55";


		new VerifyBuilder ()
				.append (
						new VerifyCn (
								new VerifyCn.Callback () {
									@Override
									public void onIllegal (String value) {

									}
								}, username))
				.append (
						new VerifyRealname (
								new VerifyRealname.Callback () {
									@Override
									public void onTooLong (String value) {

									}
								}, username))
				.append (
						new VerifyIDCard (
								new VerifyIDCard.Callback () {
									@Override
									public void onEmpty (String value) {

									}

									@Override
									public void onIllegal (String value) {

									}
								}, idCard))
				.append (
						new VerifyPassword (
								new VerifyPassword.Callback () {
									@Override
									public void onIllegal (String value) {
									}
								}, passwd))
				.execute (
						new ISuccessCallback () {
							@Override
							public void onSuccess () {

							}
						});

	}
}