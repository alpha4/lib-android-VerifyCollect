package com.bookbuf.lib_android_verifycollect;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bookbuf.verify.VerifyCn;
import com.bookbuf.verify.VerifyIDCard;
import com.bookbuf.verify.VerifyPassword;
import com.bookbuf.verify.VerifyRealname;
import com.bookbuf.verify.base.ISuccessCallback;
import com.bookbuf.verify.base.VerifyBuilder;

public class MainActivity extends AppCompatActivity {

	EditText editText_username;
	EditText editText_idcard;
	EditText editText_passwd;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate (savedInstanceState);
		setContentView (R.layout.activity_main);
		editText_username = (EditText) findViewById (R.id.username);
		editText_idcard = (EditText) findViewById (R.id.idcard);
		editText_passwd = (EditText) findViewById (R.id.passwd);

	}

	public void onVerify (View view) {
		verifyRegister ();
	}

	void toast (String value) {
		Toast.makeText (this, value, Toast.LENGTH_SHORT).show ();
	}


	void verifyRegister () {

		String username = editText_username.getText ().toString ();
		String idCard = editText_idcard.getText ().toString ();
		String passwd = editText_passwd.getText ().toString ();


		new VerifyBuilder ()
				.append (
						new VerifyCn (
								new VerifyCn.Callback () {
									@Override
									public void onIllegal (String value) {
										toast (value + "--> 非中文字符");
									}
								}, username))
				.append (
						new VerifyRealname (
								new VerifyRealname.Callback () {
									@Override
									public void onTooLong (String value) {
										toast (value + "--> 姓名过长");
									}
								}, username))
				.append (
						new VerifyIDCard (
								new VerifyIDCard.Callback () {
									@Override
									public void onEmpty (String value) {
										toast (value + "--> 为空");
									}

									@Override
									public void onIllegal (String value) {
										toast (value + "--> 身份证格式非法");
									}
								}, idCard))
				.append (
						new VerifyPassword (
								new VerifyPassword.Callback () {
									@Override
									public void onIllegal (String value) {
										toast (value + "--> 密码格式非法");
									}
								}, passwd))
				.execute (
						new ISuccessCallback () {
							@Override
							public void onSuccess () {
								toast ("验证通过");
							}
						});

	}
}
